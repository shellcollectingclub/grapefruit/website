const amplify = {
  Auth: {
    identityPoolId: 'us-east-1:15016f9e-3773-4c6c-9469-d21b3d1cad7f',
    region: 'us-east-1',
    userPoolId: 'us-east-1_nHS4ixVr3',
    userPoolWebClientId: '14ikajn43r7q2104eqvi4alk9e',
    mandatorySignIn: true,
    cookieStorage: {
      domain: '.shellcollecting.club',
      path: '/',
      expires: 365,
      secure: true
    }
  },
  API: {
    endpoints: [
      {
        name: 'test',
        endpoint: 'https://s0tkpjozy2.execute-api.us-east-1.amazonaws.com/debug',
        region: 'us-east-1'
      }
    ]
  },
  Storage: {
    bucket: 'pcaps.shellcollecting.club',
    region: 'us-east-1',
    level: 'public'
  }
}

export default amplify;
