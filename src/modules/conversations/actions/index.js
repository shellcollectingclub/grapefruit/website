// TODO: Move these 'magic strings' to a shared location for the reducers
// and the actions.
const setLimit = (limit) => ({ type: 'SET_CONVERSATIONS_TABLE_LIMIT', limit });
const setOffset = (offset) => ({ type: 'SET_CONVERSATIONS_TABLE_OFFSET', offset });
const setData = (data) => ({ type: 'SET_CONVERSATIONS_TABLE_DATA', data });
const setFullData = (data) => ({ type: 'SET_CONVERSATIONS_TABLE_FULL_DATA', data });
const setPages = (pages) => ({ type: 'SET_CONVERSATIONS_TABLE_PAGES', pages });
const setServices = (services) => ({ type: 'SET_CONVERSATIONS_TABLE_SERVICES', services });
const setSelectedService = (value) => ({ type: 'SET_CONVERSATIONS_TABLE_SERVICES_SELECT_VALUE', value });
const setRegexes = (regexes) => ({ type: 'SET_CONVERSATIONS_TABLE_REGEXES', regexes });
const setSelectedRegexes = (regexes) => ({ type: 'SET_CONVERSATIONS_TABLE_REGEXES_SELECT_VALUES', regexes });

export {
  setLimit,
  setOffset,
  setData,
  setFullData,
  setPages,
  setServices,
  setSelectedService,
  setRegexes,
  setSelectedRegexes
};
