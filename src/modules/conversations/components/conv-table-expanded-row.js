import React from 'react';

const ConversationsTableExpandedRow = ({ data }) => (
  <div style={{padding: 15}}>
    <pre style={{textAlign: 'left'}}>
      ID: {data.id}<br/>
      File Path: <a href={data.file_path} download={`${data.file_path.split('/').pop()}.pcap`}>Download</a><br/>
      File Size: {data.file_size}<br/>
      Packet Count: {data.num_packets}<br/>
      Source IP: {data.src_ip}<br/>
      Destination IP: {data.dst_ip}<br/>
      Source Port: {data.src_port}<br/>
      Destination Port: {data.dst_port}<br/>
      Round: {data.ROUND_NUM}<br/>
      Service: {data.service_id}<br/>
    </pre>
  </div>
);

export default ConversationsTableExpandedRow;
