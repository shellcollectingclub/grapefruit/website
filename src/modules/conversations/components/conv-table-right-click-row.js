import React from 'react';
import classnames from 'classnames';

const ConversationsTableRightClickRow = (props) => (
  <div
    className={classnames('rt-tr', props.className)}
    role="row"
  >
    {props.children}
  </div>
);

export default ConversationsTableRightClickRow;
