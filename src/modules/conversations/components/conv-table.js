import React from 'react';
import ReactTable from 'react-table';
import 'react-table/react-table.css';

import ConversationsTableExpandedRow from './conv-table-expanded-row.js';
import ConversationsTableRightClickRow from './conv-table-right-click-row.js';

const ConversationsTable = (props) => (
  <div style={{marginTop: 20}}>
    <ReactTable
      data={props.data}
      columns={props.columns}
      pages={props.pages}
      defaultPageSize={props.limit}
      onFetchData={props.onFetchData}
      className='-striped -highlight'
      TrComponent={ConversationsTableRightClickRow}
      getTrProps={(state, rowInfo, column, instance) => {
        return {
          rowInfo
        };
      }}
      SubComponent={(row) => (
        <ConversationsTableExpandedRow data={props.fullData[row.index]}/>
      )}
      selectedService={props.selectedService}
      showPaginationTop
      showPaginationBottom
      manual
    />
  </div>
);

export default ConversationsTable;
