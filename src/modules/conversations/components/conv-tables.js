import React from 'react';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import { Grid, Row, Col } from 'react-bootstrap';

import ConversationTable from '../containers/conv-table.js';

const ConversationsTables = (props) => (
  <Grid>
    <Row>
      <Col md={6}>
        <Select
          placeholder='Filter by service...'
          value={props.selectedService}
          onChange={(selectedService) => props.onSelectService(selectedService, props.limit, props.offset)}
          options={props.services}
        />
      </Col>
      <Col md={6}>
        <Select
          multi
          placeholder='Filter by regex...'
          value={props.selectedRegexes}
          onChange={(selectedRegexes) => props.onSelectRegexes(selectedRegexes, props.limit, props.offset)}
          options={props.regexes}
        />
      </Col>
    </Row>
    <Row>
      <ConversationTable selectedService={props.selectedService}/>
    </Row>
  </Grid>
);

export default ConversationsTables;
