import React from 'react';
import ConversationsTables from '../containers/conv-tables.js';

const Conversations = (props) => (
  <div>
    <ConversationsTables/>
  </div>
);

export default Conversations;
