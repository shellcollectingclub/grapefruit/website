import { connect } from 'react-redux';
import _ from 'lodash';

import { API } from 'aws-amplify';

import ConversationsTable from '../components/conv-table.js';
import { setLimit, setOffset, setData, setPages, setFullData } from '../actions';

import { componentDidMount } from 'react-lifecycle-hoc';

// TODO: split this into multiple files

const disp = [0, 2, 3, 6, 7, 8, 9, 10];
const all_columns = [
  {
    Header: 'ID',
    accessor: 'id'
  },
  {
    Header: 'File Path',
    accessor: 'file_path'
  },
  {
    Header: 'File Size',
    accessor: 'file_size'
  },
  {
    Header: 'Packet Count',
    accessor: 'num_packets'
  },
  {
    Header: 'Start Time',
    accessor: 'start_time'
  },
  {
    Header: 'End Time',
    accessor: 'end_time'
  },
  {
    Header: 'Source IP',
    accessor: 'src_ip'
  },
  {
    Header: 'Dest. IP',
    accessor: 'dst_ip'
  },
  {
    Header: 'Source Port',
    accessor: 'src_port'
  },
  {
    Header: 'Dest. Port',
    accessor: 'dst_port'
  },
  {
    Header: 'Round',
    accessor: 'ROUND_NUM'
  },
  {
    Header: 'Service',
    accessor: 'service_id'
  },
  {
    Header: 'Magic?',
    accessor: 'magic'
  },
  {
    Header: 'Incomplete?',
    accessor: 'maybe_incomplete'
  }
];
const columns = _.filter(all_columns, (col, idx) => _.includes(disp, idx));

// A function that dispatches a function that asynchronously dispatches other
// actions. This lets us do async things
const asyncAPIGetDispatch = (dispatch, props) => {
  dispatch(
    (dispatch) => {
      let url = `/conversations?limit=${props.limit}&offset=${props.offset}`;
      if (props.sort_by !== undefined) {
        url += `&sort_by=${props.sort_by}`;
        if (props.ordering !== undefined) {
          url += `&ordering=${props.ordering}`;
        }
      } else {
        url += `&sort_by=id&ordering=desc`
      }
      if (props.selectedService > 0) {
        url += `&service=${props.selectedService}`;
      }
      console.log(url);
      API.get('test', url).then(
        (resp) => {
          let data = resp;
          data = data.map((row) => {
            const res = {};
            _.each(all_columns, (col, idx) => {
              if (_.includes(disp, idx)) {
                res[col.accessor] = row[idx];
              }
            });
            return res;
          });
          dispatch(setData(data));

          data = resp;
          data = data.map((row) => {
            const res = {};
            _.each(all_columns, (col, idx) => {
              res[col.accessor] = row[idx];
            });
            return res;
          });
          dispatch(setFullData(data));


        },
        (err) => { console.log(err); }
      );

      let url_count = `/conversations?count_only=true`;
      if (props.selectedService > 0) {
        url_count += `&service=${props.selectedService}`;
      }
      API.get('test', url_count).then(
        (resp) => {
          let count = -1;
          try {
            count = parseInt(resp[0][0], 10);
            count = Math.ceil(count / props.limit);
            dispatch(setPages(count))
          } catch(e) {
          }
        },
        (err) => { console.log(err); }
      );
    }
  );
};

// ---------------- Initial State container (loads initial state)

const loadInitialState = ({ props }) => {
  // onDispatch is a function exposed in mapDispatchToProps to expose the dispatch
  // function for us to load some initial state when the component mounts
  const dispatch = props.onDispatch();
  asyncAPIGetDispatch(dispatch, props);
};

// Note that this container is 'leafier' in the HTML DOM than at the bottom
// of this file, because that one wraps this one.
const initialState = componentDidMount(loadInitialState)(ConversationsTable);

// ---------------- Right click menu stuff

// Add https://github.com/fkhadra/react-contexify in the future
/*const menu_items = (props) => ([
  {
    label: `Filter by Source IP: ${props.src_ip}`,
    onClick: (event, props) => { console.log('Not implemented') }
  }
]);*/

//const withContextMenu = ContextMenu(menu_items)(initialState);

// ---------------- Last container (sends data down to the dumb component from state manager)

const mapStateToProps = (state) => ({
  offset: state.conversationsTableOffset,
  limit: state.conversationsTableLimit,
  data: state.conversationsTableData,
  fullData: state.conversationsTableFullData,
  pages: state.conversationsTablePages,
  columns
});

const mapDispatchToProps = (dispatch) => ({
  onDispatch: () => dispatch,
  onFetchData: (state, instance) => {
    const newLimit = state.pageSize;
    const newOffset = Math.max(newLimit * state.page, 0);
    const props = { limit: newLimit, offset: newOffset, selectedService: instance.props.selectedService };
    if (state.sorted.length > 0) {
      props.sort_by = state.sorted[0].id;
      props.ordering = state.sorted[0].desc ? 'DESC' : 'ASC';
    }
    dispatch(setLimit(newLimit));
    dispatch(setOffset(newOffset));
    asyncAPIGetDispatch(dispatch, props);
  }
});

export { asyncAPIGetDispatch as refetchConversations };
export default connect(mapStateToProps, mapDispatchToProps)(initialState);
