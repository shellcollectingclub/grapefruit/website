import { connect } from 'react-redux';
import _ from 'lodash';
import { componentDidMount } from 'react-lifecycle-hoc';

import { API } from 'aws-amplify';

import ConversationsTables from '../components/conv-tables.js';
import { setServices, setSelectedService, setRegexes, setSelectedRegexes } from '../actions';
import { refetchConversations } from './conv-table.js';

const disp_services = [0,1,2,3,4];
const all_columns_services = [
  {
    Header: 'ID',
    accessor: 'id'
  },
  {
    Header: 'Service Name',
    accessor: 'service_name'
  },
  {
    Header: 'IP',
    accessor: 'ip'
  },
  {
    Header: 'Port',
    accessor: 'port_num'
  },
  {
    Header: 'Protocol',
    accessor: 'protocol'
  }
];

const disp_regexes = [0,1,2];
const all_columns_regexes = [
  {
    Header: 'ID',
    accessor: 'id'
  },
  {
    Header: 'Pattern',
    accessor: 'pattern'
  },
  {
    Header: 'Description',
    accessor: 'description'
  }
];

// A function that dispatches a function that asynchronously dispatches other
// actions. This lets us do async things
const asyncAPIGetDispatch = (dispatch, props) => {
  dispatch(
    (dispatch) => {
      let url = `/service`;
      API.get('test', url).then(
        (resp) => {
          let data = resp;
          data = data.map((row) => {
            const res = {};
            _.each(all_columns_services, (col, idx) => {
              if (_.includes(disp_services, idx)) {
                res[col.accessor] = row[idx];
              }
            });
            return res;
          });
          
          dispatch(setServices(data));
        },
        (err) => { console.log(err); }
      );
      
      url = `/regex`;
      API.get('test', url).then(
        (resp) => {
          let data = resp;
          data = data.map((row) => {
            const res = {};
            _.each(all_columns_regexes, (col, idx) => {
              if (_.includes(disp_regexes, idx)) {
                res[col.accessor] = row[idx];
              }
            });
            return res;
          });
          
          dispatch(setRegexes(data));
        },
        (err) => { console.log(err); }
      );
    }
  );
};

// ---------------- Initial State container (loads initial state)

const loadInitialState = ({ props }) => {
  // onDispatch is a function exposed in mapDispatchToProps to expose the dispatch
  // function for us to load some initial state when the component mounts
  const dispatch = props.onDispatch();
  asyncAPIGetDispatch(dispatch, props);
};

// Note that this container is 'leafier' in the HTML DOM than at the bottom
// of this file, because that one wraps this one.
const initialState = componentDidMount(loadInitialState)(ConversationsTables);

// ---------------- Right click menu stuff

// Add https://github.com/fkhadra/react-contexify in the future
/*const menu_items = (props) => ([
  {
    label: `Filter by Source IP: ${props.src_ip}`,
    onClick: (event, props) => { console.log('Not implemented') }
  }
]);*/

//const withContextMenu = ContextMenu(menu_items)(initialState);

// ---------------- Last container (sends data down to the dumb component from state manager)

const mapStateToProps = (state) => ({
  services: state.conversationsTableServices.map(
    (service) => ({
      value: service.id,
      label: `${service.service_name} ::: ${service.protocol} port ${service.port_num}`
    })),
  regexes: state.conversationsTableRegexes.map(
    (regex) => ({
      value: regex.id,
      label: `${regex.description} ::: ${regex.pattern}`
    })),
  selectedService: state.conversationsTableServicesSelectValue,
  selectedRegexes: state.conversationsTableRegexesSelectValues,
  offset: state.conversationsTableOffset,
  limit: state.conversationsTableLimit
});

const mapDispatchToProps = (dispatch) => ({
  onDispatch: () => dispatch,
  onSelectService: (selectedService, limit, offset) => {
    if (selectedService === null) selectedService = { value: '0' };
    dispatch(setSelectedService(selectedService.value))

    const props = {
      selectedService: selectedService.value,
      limit,
      offset
    }
    refetchConversations(dispatch, props);
  },
  onSelectRegexes: (selectedRegexes, limit, offset) => {
    dispatch(setSelectedRegexes(selectedRegexes));
    console.log('Now what?');
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(initialState);
