import React from 'react';

import Amplify from 'aws-amplify';
import { Authenticator } from 'aws-amplify-react';
import amplify from '../../../config/config.js';

Amplify.configure(amplify);

import ProviderApp from './app-reduxed.js';
import Login from './login.js';

const AuthedApp = (props) => (
  <Authenticator hideDefault={true}>
    <ProviderApp {...props}/>
    <Login history={props.history}/>
  </Authenticator>
);

export default AuthedApp;
