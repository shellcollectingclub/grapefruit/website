import React from 'react';

import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import reducers from '../lib/reducers';

import App from './app.js';

const store = createStore(reducers, applyMiddleware(thunk));

const ProviderApp = (props) => (
  <Provider store={store}>
    <App {...props}/>
  </Provider>
);

export default ProviderApp;
