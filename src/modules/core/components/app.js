import React from 'react';
import { Link } from 'react-router-dom';
import { Nav, NavItem, Navbar } from 'react-bootstrap';
import { Auth } from 'aws-amplify';

import Routes from '../routes.js'
import '../style/app.css';

const App = (props) => (
  <div className='App container'>
    <Navbar fluid collapseOnSelect>
      <Navbar.Header>
        <Navbar.Brand>
          <Link to='/'>Grapefruit</Link>
        </Navbar.Brand>
        <Navbar.Toggle />
      </Navbar.Header>
      <Navbar.Collapse>
        <Nav pullRight>
          <NavItem onClick={() => props.history.push('/dash')}>Dashboard</NavItem>
          <NavItem onClick={() => props.history.push('/conversations')}>Conversations</NavItem>
          <NavItem onClick={() => props.history.push('/regex-builder')}>Regex Builder</NavItem>

          { props.authState === 'signedIn' ?
            <NavItem onClick={() => {
              Auth.signOut().then(() => {
                props.onStateChange('signedOut');
                props.history.push('/');
              })
            }}>Logout</NavItem> : null }
        </Nav>
      </Navbar.Collapse>
    </Navbar>

    { props.authState === 'signedIn' ?
      <Routes childProps={props}/> : null }
  </div>
);

export default App;
