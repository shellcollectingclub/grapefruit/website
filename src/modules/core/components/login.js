import React, { Component } from 'react';
import { Button, FormGroup, FormControl, ControlLabel } from 'react-bootstrap';
import { Auth } from 'aws-amplify';
import _ from 'lodash';

import '../style/login.css';

// TODO: Convert these over to something like react-forms to get rid of state in this class
class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      password: '',
      userAttr: '',
      newpassword: '',
      newPasswordRequired: false,
      session: ''
    };
  }

  login(username, password) {
    Auth.signIn(username, password)
      .then(user => {
          if (user.challengeName === 'SMS_MFA') {
              this.props.onStateChange('confirmSignIn', user);
          } else if (user.challengeName === 'NEW_PASSWORD_REQUIRED') {
              this.props.onStateChange('requireNewPassword', user);
          } else {
              this.props.onStateChange('signedIn');
              this.props.history.push('/dash');
          }
      })
      .catch(err => {
          console.error(err)
      });
  }

  processNewPassword(email, password, newpassword) {
  }

  validateForm() {
    return this.state.username.length > 0 && this.state.password.length >= 6;
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }

  handleSubmit = async event => {
    event.preventDefault();

    try {
      if (!this.state.newPasswordRequired) {
	      console.log('Trying login')
        await this.login(this.state.username, this.state.password);
      } else {
	      console.log('Trying to change password')
        await this.processNewPassword(this.state.username, this.state.password, this.state.newpassword);
      }
      this.props.history.push('/dash');
    } catch (e) {
      if (e.reason==='newPassword') {
	      console.log('Need new password')
        this.setState({newPasswordRequired:true, userAttr: e.data.userAttr})
      } else {
        alert(JSON.stringify(e))
      }
    }
  }

  render() {
    if (!_.includes(['signIn', 'signedOut', 'signedUp'], this.props.authState)) return null;
    return (
      <div className='Login'>
        <form onSubmit={this.handleSubmit}>
          <FormGroup controlId='username' bsSize='large'>
            <ControlLabel>Username</ControlLabel>
            <FormControl
              autoFocus
              type='username'
              value={this.state.username}
              onChange={this.handleChange}
            />
          </FormGroup>
          <FormGroup controlId='password' bsSize='large'>
            <ControlLabel>Password</ControlLabel>
            <FormControl
              value={this.state.password}
              onChange={this.handleChange}
              type='password'
            />
          </FormGroup>
          {this.state.newPasswordRequired
            ? [ 
            <FormGroup controlId='newpassword' bsSize='large'>
              <ControlLabel>New Password</ControlLabel>
              <FormControl
                value={this.state.newpassword}
                onChange={this.handleChange}
                type='newpassword'
              />
            </FormGroup>
            ]
           : [] }
          <Button
            block
            bsSize='large'
            disabled={!this.validateForm()}
            type='submit'
          >
            Login
          </Button>
        </form>
      </div>
    );
  }
}

export default Login;
