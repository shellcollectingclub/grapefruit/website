import React from 'react';

const Lost = () => (
  <div className="Lost">
    <h3>Sorry, page not found!</h3>
  </div>
);

export default Lost;
