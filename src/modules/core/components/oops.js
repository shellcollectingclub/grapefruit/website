import React from 'react';

const Oops = () => (
  <div>
    <h2>Uh oh!</h2>
    <p>Seems like something went wrong. Try logging in again...</p>
  </div>
);

export default Oops;
