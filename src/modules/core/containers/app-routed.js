import { withRouter } from 'react-router-dom';
import AuthedApp from '../components/app-authed.js';

export default withRouter(AuthedApp);
