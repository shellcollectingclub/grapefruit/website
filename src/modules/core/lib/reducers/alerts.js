const Alerts = (state = [], action) => {
  switch (action.type) {
      case 'SET_ALERTS':
      return action.alerts
    default:
      return state
  }
}

export default Alerts;
