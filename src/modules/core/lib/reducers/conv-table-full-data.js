const conversationsTableData = (state = [], action) => {
  switch (action.type) {
    case 'SET_CONVERSATIONS_TABLE_FULL_DATA':
      return action.data;
    default:
      return state;
  }
}

export default conversationsTableData;
