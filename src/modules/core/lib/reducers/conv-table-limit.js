const conversationsTableLimit = (state = 25, action) => {
  switch (action.type) {
    case 'SET_CONVERSATIONS_TABLE_LIMIT':
      return action.limit;
    default:
      return state;
  }
}

export default conversationsTableLimit;
