const conversationsTableOffset = (state = 0, action) => {
  switch (action.type) {
    case 'SET_CONVERSATIONS_TABLE_OFFSET':
      return action.offset;
    default:
      return state;
  }
}

export default conversationsTableOffset;
