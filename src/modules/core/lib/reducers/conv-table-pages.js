const conversationsTablePages = (state = -1, action) => {
  switch (action.type) {
    case 'SET_CONVERSATIONS_TABLE_PAGES':
      return action.pages;
    default:
      return state;
  }
}

export default conversationsTablePages;
