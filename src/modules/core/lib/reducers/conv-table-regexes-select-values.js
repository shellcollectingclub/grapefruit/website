const conversationsTableServicesSelectValue = (state = [], action) => {
  switch (action.type) {
    case 'SET_CONVERSATIONS_TABLE_REGEXES_SELECT_VALUES':
      return action.regexes;
    default:
      return state;
  }
}

export default conversationsTableServicesSelectValue;
