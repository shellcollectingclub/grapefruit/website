const conversationsTableRegexes = (state = [], action) => {
  switch (action.type) {
    case 'SET_CONVERSATIONS_TABLE_REGEXES':
      return action.regexes;
    default:
      return state;
  }
}

export default conversationsTableRegexes;
