const conversationsTableServicesSelectValue = (state = '0', action) => {
  switch (action.type) {
    case 'SET_CONVERSATIONS_TABLE_SERVICES_SELECT_VALUE':
      return action.value;
    default:
      return state;
  }
}

export default conversationsTableServicesSelectValue;
