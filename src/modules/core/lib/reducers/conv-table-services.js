const conversationsTableServices = (state = [], action) => {
  switch (action.type) {
    case 'SET_CONVERSATIONS_TABLE_SERVICES':
      return action.services;
    default:
      return state;
  }
}

export default conversationsTableServices;
