const ConversationsCount = (state = [], action) => {
  switch (action.type) {
      case 'SET_CONVERSATIONS_COUNT':
      return action.count
    default:
      return state
  }
}

export default ConversationsCount;
