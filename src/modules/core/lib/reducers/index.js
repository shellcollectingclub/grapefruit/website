import { combineReducers } from 'redux';

import refreshCount from './refresh-count.js';
import alerts from './alerts.js';
import error from './error.js';
import conversationsCount from './conversations-count.js';
import conversationsTableOffset from './conv-table-offset.js';
import conversationsTableLimit from './conv-table-limit.js';
import conversationsTableData from './conv-table-data.js';
import conversationsTableFullData from './conv-table-full-data.js';
import conversationsTablePages from './conv-table-pages.js';
import conversationsTableServices from './conv-table-services.js';
import conversationsTableServicesSelectValue from './conv-table-services-select-value.js';
import conversationsTableRegexes from './conv-table-regexes.js';
import conversationsTableRegexesSelectValues from './conv-table-regexes-select-values.js';
import regexBuilderRegexesSelectValue from './regex-builder-regexes-select-value.js';
import regexBuilderRegexId from './regex-builder-regex-id.js';
import regexBuilderRegexPattern from './regex-builder-regex-pattern.js';
import regexBuilderRegexDescription from './regex-builder-regex-description.js';

const reducers = combineReducers({
  refreshCount,
  alerts,
  error,
  conversationsCount,
  conversationsTableOffset,
  conversationsTableLimit,
  conversationsTableData,
  conversationsTableFullData,
  conversationsTablePages,
  conversationsTableServices,
  conversationsTableServicesSelectValue,
  conversationsTableRegexes,
  conversationsTableRegexesSelectValues,
  regexBuilderRegexesSelectValue,
  regexBuilderRegexId,
  regexBuilderRegexPattern,
  regexBuilderRegexDescription
});

export default reducers;
