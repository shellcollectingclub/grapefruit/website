const refreshCount = (state = 0, action) => {
  switch (action.type) {
    case 'REFRESH_INCREMENT':
      return state + 1
    default:
      return state
  }
}

export default refreshCount;
