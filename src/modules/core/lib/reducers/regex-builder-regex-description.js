const regexBuilderRegexDescription = (state = '', action) => {
  switch (action.type) {
    case 'SET_REGEX_BUILDER_REGEX_DESCRIPTION':
      return action.value;
    default:
      return state;
  }
}

export default regexBuilderRegexDescription;
