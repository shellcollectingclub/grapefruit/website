const regexBuilderRegexId = (state = '', action) => {
  switch (action.type) {
    case 'SET_REGEX_BUILDER_REGEX_ID':
      return action.value;
    default:
      return state;
  }
}

export default regexBuilderRegexId;
