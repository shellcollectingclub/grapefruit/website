const regexBuilderRegexPattern = (state = '', action) => {
  switch (action.type) {
    case 'SET_REGEX_BUILDER_REGEX_PATTERN':
      return action.value;
    default:
      return state;
  }
}

export default regexBuilderRegexPattern;
