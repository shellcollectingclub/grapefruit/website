const regexBuilderRegexesSelectValue = (state = '0', action) => {
  switch (action.type) {
    case 'SET_REGEX_BUILDER_REGEXES_SELECT_VALUE':
      return action.value;
    default:
      return state;
  }
}

export default regexBuilderRegexesSelectValue;
