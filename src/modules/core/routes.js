import React from 'react';
import { Route, Switch } from 'react-router-dom';
import AppliedRoute from './components/applied-route.js';

import Lost from './components/lost.js';
import Home from '../home/components/home.js';
import Dashboard from '../dashboard/components/dashboard.js';
import Conversations from '../conversations/components/conversations.js';
import RegexBuilder from '../regex-builder/containers/regex-builder.js';

const Routes = ({ childProps }) => (
  <Switch>
    <AppliedRoute path='/' exact component={Home} props={childProps} />
    <AppliedRoute path='/dash' exact component={Dashboard} props={childProps} />
    <AppliedRoute path='/conversations' exact component={Conversations} props={childProps} />
    <AppliedRoute path='/regex-builder' exact component={RegexBuilder} props={childProps} />
    <Route component={Lost} />
  </Switch>
);

export default Routes;
