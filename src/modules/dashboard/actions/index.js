// TODO: Move these 'magic strings' to a shared location for the reducers
// and the actions.
const refreshCount = { type: 'REFRESH_INCREMENT' };
const setError = (error) => ({ type: 'SET_ERRORS', error });
const setAlerts = (alerts) => ({ type: 'SET_ALERTS', alerts });
const setConversationsCount = (count) => ({ type: 'SET_CONVERSATIONS_COUNT', count });

export {
  refreshCount,
  setError,
  setAlerts,
  setConversationsCount
};
