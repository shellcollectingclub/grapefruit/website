import React from 'react';

const Alerts = (props) => (
  <div className='Dash-Alerts'>
    { props.error && props.error !== '' ?
      <div class='alert alert-danger' role='alert'>
        {props.error}
      </div>
      : null
    }

    { props.alerts && props.alerts.length !== 0 ?
      props.alerts.map((result, index) => (
        <div key={index} className='alert alert-info' role='alert'>
          Id: {result[0]}, Msg: {result[1]}
        </div>
      ))
      : null
    }
  </div>
);

export default Alerts;
