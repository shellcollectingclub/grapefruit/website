import React from 'react';
import Loading from 'react-loading';

const ConversationsCard = (props) => (
  <div>
    { console.log(props) }
    { props.conversationsCount && props.conversationsCount !== -1 ?
      <span>There are currently <a href='/conversations'>{props.conversationsCount}</a> conversations that have been processed</span> :
      <center><Loading type='bars' color='#777'/></center>
    }
  </div>
);

export default ConversationsCard;
