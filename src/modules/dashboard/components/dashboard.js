import React from 'react';

import Alerts from '../containers/alerts.js';
import ConversationsCard from '../containers/conversations-card.js';

const Dashboard = (props) => (
  <div className='Dashboard'>
    <div className='lander'>
      <h1>{props.authData.getUsername()} - Dashboard</h1>
      <p>Enhance the Experience</p>

      <Alerts/>
      <ConversationsCard/>

    </div>
  </div>
);

export default Dashboard;
