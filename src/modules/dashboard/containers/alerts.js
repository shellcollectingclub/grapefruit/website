import { connect } from 'react-redux';
import { componentDidMount } from 'react-lifecycle-hoc';

import Alerts from '../components/alerts.js';

// ---------------- First container (loads initial state)

const loadInitialState = ({ props }) => {
  // onDispatch is a function exposed in mapDispatchToProps to expose the dispatch
  // function for us to load some initial state when the component mounts
};

// Note that this container is 'leafier' in the HTML DOM than at the bottom
// of this file, because that one wraps this one.
const initialStateAlerts = componentDidMount(loadInitialState)(Alerts);

// ---------------- Second container (sends data down to the dumb component from state manager)

const mapStateToProps = (state) => ({
  alerts: state.alerts,
  error: state.error
});

const mapDispatchToProps = (dispatch) => ({
  onDispatch: () => dispatch
});

export default connect(mapStateToProps, mapDispatchToProps)(initialStateAlerts);
