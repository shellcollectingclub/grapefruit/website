import { connect } from 'react-redux';

import { API } from 'aws-amplify';

import ConversationsCard from '../components/conversations-card.js';
import { setConversationsCount, setError } from '../actions';

import { componentDidMount } from 'react-lifecycle-hoc';

// A function that dispatches a function that asynchronously dispatches other
// actions. This lets us do async things
const asyncAPIGetDispatch = (dispatch) => {
  dispatch(
    (dispatch) => {
      API.get('test', '/conversations?count_only=true').then(
        (resp) => {
          let count = -1;
          try {
            count = parseInt(resp[0][0], 10);
            dispatch(setConversationsCount(count))
          } catch(e) {
            dispatch(setError(`Failed to parse int from ${resp}`));
          }
        },
        (err) => { console.log(err); dispatch(setError('Failure retrieving data from API')) }
      );
    }
  );
};

// ---------------- First container (loads initial state)

const loadInitialState = ({ props }) => {
  // onDispatch is a function exposed in mapDispatchToProps to expose the dispatch
  // function for us to load some initial state when the component mounts
  const dispatch = props.onDispatch();
  dispatch(setConversationsCount(-1));
  asyncAPIGetDispatch(dispatch);
};

// Note that this container is 'leafier' in the HTML DOM than at the bottom
// of this file, because that one wraps this one.
const initialState = componentDidMount(loadInitialState)(ConversationsCard);

// ---------------- Second container (sends data down to the dumb component from state manager)

const mapStateToProps = (state) => ({
  conversationsCount: state.conversationsCount
});

const mapDispatchToProps = (dispatch) => ({
  onDispatch: () => dispatch
});

export default connect(mapStateToProps, mapDispatchToProps)(initialState);
