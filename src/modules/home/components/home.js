import React from 'react';

const Home = () => (
  <div className="Home">
    <div className="lander">
      <h1>Grapefruit</h1>
      <p>Enhance the Experience</p>
    </div>
  </div>
);

export default Home;
