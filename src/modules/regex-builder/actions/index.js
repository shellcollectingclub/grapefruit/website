const setSelectedRegex = (value) => ({ type: 'SET_REGEX_BUILDER_REGEXES_SELECT_VALUE', value });
const setRegexId = (value) => ({ type: 'SET_REGEX_BUILDER_REGEX_ID', value });
const setRegexPattern = (value) => ({ type: 'SET_REGEX_BUILDER_REGEX_PATTERN', value });
const setRegexDescription = (value) => ({ type: 'SET_REGEX_BUILDER_REGEX_DESCRIPTION', value });

export {
  setSelectedRegex,
  setRegexId,
  setRegexPattern,
  setRegexDescription
};

