import React from 'react';
import { Grid, Row, Col, FormGroup, FormControl, ControlLabel, ButtonToolbar, Button } from 'react-bootstrap';
import Select from 'react-select';

const RegexBuilder = (props) => (
  <div style={{textAlign: 'left'}}>
    <Grid>
      <Row>
        <Col md={6}>
          <Select
            placeholder='Load regex...'
            value={props.selectedRegex}
            onChange={(selected) => props.onSelectRegex(selected, props.rawRegexes)}
            options={props.regexes}
          />
        </Col>
      </Row>
      <br/>
      <Row>
        <Col md={6}>
          <form>
            { props.selectedRegex !== '0' ?
              <FormGroup controlId='regex-id'>
                <ControlLabel>Regex ID: </ControlLabel>
                <FormControl
                  type='text'
                  value={props.selectedRegex}
                  onChange={props.onRegexIdChange}
                  placeholder='ID'
                  disabled
                />
              </FormGroup> : null
            }

            <FormGroup controlId='regex-pattern'>
              <ControlLabel>Pattern: </ControlLabel>
              <FormControl
                type='text'
                value={props.regexPattern}
                onChange={props.onRegexPatternChange}
                placeholder='Pattern...'
              />
            </FormGroup>
            
            <FormGroup controlId='regex-description'>
              <ControlLabel>Description: </ControlLabel>
              <FormControl
                type='text'
                value={props.regexDescription}
                onChange={props.onRegexDescriptionChange}
                placeholder='Description...'
              />
            </FormGroup>

            { props.selectedRegex !== '0' ?
              <ButtonToolbar>
                <Button bsStyle='warning' onClick={(e) => { e.preventDefault(); props.onUpdateClicked(props); }}>Update</Button>
                <Button bsStyle='danger' onClick={(e) => { e.preventDefault(); props.onDeleteClicked(props); }}>Delete</Button> 
              </ButtonToolbar> :
              <ButtonToolbar>
                <Button bsStyle='success' onClick={(e) => { e.preventDefault(); props.onInsertClicked(props); }}>Insert</Button>
              </ButtonToolbar>
            }
          </form>
        </Col>
      </Row>
    </Grid>
  </div>
);

export default RegexBuilder;
