import { connect } from 'react-redux';
import _ from 'lodash';
import { componentDidMount } from 'react-lifecycle-hoc';

import { API } from 'aws-amplify';

import { setRegexes } from '../../conversations/actions';
import { setSelectedRegex, setRegexId, setRegexPattern, setRegexDescription } from '../actions';

import RegexBuilder from '../components/regex-builder.js';

const disp_regexes = [0,1,2];
const all_columns_regexes = [
  {
    Header: 'ID',
    accessor: 'id'
  },
  {
    Header: 'Pattern',
    accessor: 'pattern'
  },
  {
    Header: 'Description',
    accessor: 'description'
  }
];

// A function that dispatches a function that asynchronously dispatches other
// actions. This lets us do async things
const asyncAPIGetDispatch = (dispatch, props) => {
  dispatch(
    (dispatch) => {
      let url = `/regex`;
      API.get('test', url).then(
        (resp) => {
          let data = resp;
          data = data.map((row) => {
            const res = {};
            _.each(all_columns_regexes, (col, idx) => {
              if (_.includes(disp_regexes, idx)) {
                res[col.accessor] = row[idx];
              }
            });
            return res;
          });
          
          dispatch(setRegexes(data));
        },
        (err) => { console.log(err); }
      );
    }
  );
};

// ---------------- Initial State container (loads initial state)

const loadInitialState = ({ props }) => {
  // onDispatch is a function exposed in mapDispatchToProps to expose the dispatch
  // function for us to load some initial state when the component mounts
  const dispatch = props.onDispatch();
  asyncAPIGetDispatch(dispatch, props);
};

// Note that this container is 'leafier' in the HTML DOM than at the bottom
// of this file, because that one wraps this one.
const initialState = componentDidMount(loadInitialState)(RegexBuilder);

// ---------------- Last container (sends data down to the dumb component from state manager)

const mapStateToProps = (state) => ({
  regexes: state.conversationsTableRegexes.map(
    (regex) => ({
      value: regex.id,
      label: `${regex.description} ::: ${regex.pattern}`
    })),
  rawRegexes: state.conversationsTableRegexes,
  selectedRegex: state.regexBuilderRegexesSelectValue,
  regexId: state.regexBuilderRegexId,
  regexPattern: state.regexBuilderRegexPattern,
  regexDescription: state.regexBuilderRegexDescription
});

const mapDispatchToProps = (dispatch) => ({
  onDispatch: () => dispatch,
  onSelectRegex: (selectedRegex, rawRegexes) => {
    if (selectedRegex === null) selectedRegex = { value: '0' };
    if (selectedRegex.value !== '0') {
      const regexIndex = _.findIndex(rawRegexes, (regex) => regex.id === selectedRegex.value);
      dispatch(setRegexPattern(rawRegexes[regexIndex].pattern));
      dispatch(setRegexDescription(rawRegexes[regexIndex].description));
    }
    dispatch(setSelectedRegex(selectedRegex.value));
  },
  onRegexIdChange: (e) => dispatch(setRegexId(e.target.value)),
  onRegexPatternChange: (e) => dispatch(setRegexPattern(e.target.value)),
  onRegexDescriptionChange: (e) => dispatch(setRegexDescription(e.target.value)),
  onUpdateClicked: (props) => {
    const data = {
      body: {
        action: 'update',
        id: props.selectedRegex,
        pattern: props.regexPattern,
        desc: props.regexDescription
      }
    }

    API.post('test', '/regex', data).then(
      (resp) => asyncAPIGetDispatch(dispatch),
      (err) => console.log(err)
    );
  },
  onDeleteClicked: (props) => {
    const data = {
      body: {
        action: 'delete',
        id: props.selectedRegex
      }
    }

    API.post('test', '/regex', data).then(
      (resp) => asyncAPIGetDispatch(dispatch),
      (err) => console.log(err)
    );
  },
  onInsertClicked: (props) => {
    const data = {
      body: {
        action: 'insert',
        pattern: props.regexPattern,
        desc: props.regexDescription
      }
    }

    API.post('test', '/regex', data).then(
      (resp) => asyncAPIGetDispatch(dispatch),
      (err) => console.log(err)
    );
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(initialState);
